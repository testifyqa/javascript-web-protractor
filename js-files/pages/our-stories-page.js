"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_page_1 = require("./base-page");
const protractor_1 = require("protractor");
class OurStoriesPage extends base_page_1.BasePage {
    constructor() {
        super('Our stories');
        this.viewAllStoriesButton = protractor_1.element(protractor_1.by.css("a[title='View all stories']"));
    }
}
exports.OurStoriesPage = OurStoriesPage;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib3VyLXN0b3JpZXMtcGFnZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3BhZ2VzL291ci1zdG9yaWVzLXBhZ2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBQSwyQ0FBdUM7QUFDdkMsMkNBQWlFO0FBRWpFLE1BQWEsY0FBZSxTQUFRLG9CQUFRO0lBSXhDO1FBQ0ksS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxvQkFBTyxDQUFDLGVBQUUsQ0FBQyxHQUFHLENBQUMsNkJBQTZCLENBQUMsQ0FBQyxDQUFDO0lBQy9FLENBQUM7Q0FDSjtBQVJELHdDQVFDIn0=