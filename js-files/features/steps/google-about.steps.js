"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cucumber_1 = require("cucumber");
const protractor_1 = require("protractor");
const base_page_1 = require("../../pages/base-page");
const our_stories_page_1 = require("../../pages/our-stories-page");
const chai_1 = __importDefault(require("chai"));
let expect = chai_1.default.expect;
let basePage;
let ourStoriesPage;
cucumber_1.Given('a Google fan is on {string}', (googleUrl) => __awaiter(void 0, void 0, void 0, function* () {
    yield protractor_1.browser.get(googleUrl);
}));
cucumber_1.When('they navigate to {string}', (linkText) => __awaiter(void 0, void 0, void 0, function* () {
    basePage = new base_page_1.BasePage(linkText);
    yield basePage.navLink.click();
}));
cucumber_1.Then('they see information about Google\'s background story', () => __awaiter(void 0, void 0, void 0, function* () {
    ourStoriesPage = new our_stories_page_1.OurStoriesPage();
    yield expect(ourStoriesPage.viewAllStoriesButton.isDisplayed());
}));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ29vZ2xlLWFib3V0LnN0ZXBzLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vZmVhdHVyZXMvc3RlcHMvZ29vZ2xlLWFib3V0LnN0ZXBzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTZDO0FBQzdDLDJDQUFxQztBQUNyQyxxREFBaUQ7QUFDakQsbUVBQThEO0FBQzlELGdEQUF3QjtBQUV4QixJQUFJLE1BQU0sR0FBRyxjQUFJLENBQUMsTUFBTSxDQUFDO0FBRXpCLElBQUksUUFBaUIsQ0FBQztBQUN0QixJQUFJLGNBQTZCLENBQUM7QUFFbEMsZ0JBQUssQ0FBQyw2QkFBNkIsRUFBRSxDQUFNLFNBQVMsRUFBRSxFQUFFO0lBQ3BELE1BQU0sb0JBQU8sQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUM7QUFDakMsQ0FBQyxDQUFBLENBQUMsQ0FBQztBQUVILGVBQUksQ0FBQywyQkFBMkIsRUFBRSxDQUFNLFFBQVEsRUFBRSxFQUFFO0lBQ2hELFFBQVEsR0FBRyxJQUFJLG9CQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDbEMsTUFBTSxRQUFRLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFDO0FBQ25DLENBQUMsQ0FBQSxDQUFDLENBQUM7QUFFSCxlQUFJLENBQUMsdURBQXVELEVBQUUsR0FBUSxFQUFFO0lBQ3BFLGNBQWMsR0FBRyxJQUFJLGlDQUFjLEVBQUUsQ0FBQztJQUN0QyxNQUFNLE1BQU0sQ0FBQyxjQUFjLENBQUMsb0JBQW9CLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztBQUNwRSxDQUFDLENBQUEsQ0FBQyxDQUFDIn0=