import { Given, When, Then } from "cucumber";
import { browser } from "protractor";
import { BasePage } from "../../pages/base-page";
import { OurStoriesPage } from "../../pages/our-stories-page";
import chai from "chai";

let expect = chai.expect;

let basePage:BasePage;
let ourStoriesPage:OurStoriesPage;

Given('a Google fan is on {string}', async(googleUrl) => {
    await browser.get(googleUrl);
});

When('they navigate to {string}', async(linkText) => {
    basePage = new BasePage(linkText);
    await basePage.navLink.click();
});

Then('they see information about Google\'s background story', async() => {
    ourStoriesPage = new OurStoriesPage();
    await expect(ourStoriesPage.viewAllStoriesButton.isDisplayed());
});