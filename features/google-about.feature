Feature: Google About

  As a Google fan,
  I want to find out more information about Google,
  so that I can learn where they came from

  Scenario: 01. Navigating to 'Our stories'
      Given a Google fan is on "https://about.google"
      When they navigate to "Our stories"
      Then they see information about Google's background story