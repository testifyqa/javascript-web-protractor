import { Config } from "protractor"; //import the Configuration object from protractor, so we can use it

export let config: Config = { //create an object of Config called 'config' and export it so it is accessible by other files
    directConnect: true, //Your test script communicates directly Chrome Driver or Firefox Driver, bypassing any Selenium Server.
    capabilities: {
        browserName: 'chrome' //set test browser to 'chrome', can also be set to 'firefox' as well as some others
    },
    specs: ['./features/*.feature'], // set the location of our tests to any and all feature files in the 'features' folder
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'), //this module needs to be called as a custom framework
    cucumberOpts: {
        require: ['./features/steps/*.steps.ts'] //require our step definition files
    }
};