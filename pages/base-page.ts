import { ElementFinder, element, by } from "protractor";

export class BasePage {

    navLink:ElementFinder;

    constructor(linkTitleText:string) {
        this.navLink = element(by.css("a[title='"+linkTitleText+"']"));
    }
}