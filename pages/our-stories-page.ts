import { BasePage } from "./base-page";
import { browser, ElementFinder, element, by } from "protractor";

export class OurStoriesPage extends BasePage {

    viewAllStoriesButton:ElementFinder;

    constructor() {
        super('Our stories');
        this.viewAllStoriesButton = element(by.css("a[title='View all stories']"));
    }
}